addSbtPlugin("com.typesafe.sbt" % "sbt-twirl" % "1.5.1")
addSbtPlugin("com.earldouglas" % "xsbt-web-plugin" % "4.2.4")


libraryDependencies ++= Seq(
  "org.eclipse.jetty" % "jetty-webapp" % "9.4.43.v20210629" % "container",
  "org.eclipse.jetty" % "jetty-plus"   % "9.4.43.v20210629" % "container"
)
