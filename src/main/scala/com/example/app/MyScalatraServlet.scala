package com.example.app

import org.scalatra._

class MyScalatraServlet extends ScalatraServlet {

  post("/users") {
    "usuario creado"
  }

  get("/users/:id") {
    "usuario con id" + params("id")
  }

  get("/users") {
    "lista de users"
  }

  put("/users/:id") {
    "usuario remplazado con id:" + params("id")
  }

  patch("/users/:id") {
    "usuario actualizado con id:" + params("id")
  }

  delete("/users/:id") {
    "elimina mi usuario con el id:" + params("id")
  }
}

